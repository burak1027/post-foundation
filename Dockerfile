FROM openjdk:16-alpine3.13
COPY target/Post-foundation-0.0.1-SNAPSHOT.jar Post-foundation-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/Post-foundation-0.0.1-SNAPSHOT.jar"]
EXPOSE 8082
