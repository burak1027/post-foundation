package com.burak.postfoundation.domain.model.dto;

import com.burak.postfoundation.domain.model.entity.Article;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.validation.constraints.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class PostDto {
    @Positive
    long id;
    @Positive
    long postCreatorId;
    @Positive
    long categoryId;
    @NotBlank
    @NotNull
    String permaLink;
    @NotBlank
    @Size(max = 200)
    private String title;
    //private String content;
    private Article article;
    //@ApiModelProperty(value = "dateCreated", example = "2021-21-05 13:30:41")
    @ApiModelProperty(required = true, dataType = "org.joda.time.LocalDate", example = "2021-21-05 13:30:41")
    //@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss",iso = DateTimeFormat.ISO.DATE_TIME)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @PastOrPresent
    //@Null
    private Date dateCreated;
    // @ApiModelProperty(value = "dateCreated", example = "2021-21-05 13:30:41")
    @ApiModelProperty(required = true, dataType = "org.joda.time.LocalDate", example = "2021-21-05 13:30:41")
    //@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @PastOrPresent
    private Date lastUpdated;
    @NotNull
    private boolean isDeleted;
    @NotNull
    private boolean isPrivate;
    @Min(value = 0)
    private int likeCounter;
    @Min(value = 0)
    private int timesViewed;

    //private String authorName;
}
