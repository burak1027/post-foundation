package com.burak.postfoundation.domain.model.entity;

import jdk.jfr.Enabled;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
@Table(name = "Post")
@Getter
@Setter
@ToString
@SQLDelete(sql = "UPDATE post SET deleted = true WHERE post_id=?")
@Where(clause = "deleted=false")

public class PostEntity {
    @Id
    @Column(name = "post_id")
    private long id;
    @Column(name = "post_creator_id")
    private long postCreatorId;
    @Column(name = "category_id")
    long categoryId;
    @Column(name = "Title")
    private String title;
    @Column(name="permalink",unique = true)
    String permaLink;
    //@Column(name = "content")
    //private String content;
    @Column(name = "date_created")
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date dateCreated;
    @Column(name = "date_updated")
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date lastUpdated;
    @Column(name = "deleted")
    private boolean isDeleted;
    @Column(name = "is_private")
    private boolean isPrivate;
    @Column(name = "like_count")
    private int likeCounter;
    @Column(name = "times_viewed")
    private int timesViewed;
    @AttributeOverrides(
            @AttributeOverride(name = "content", column = @Column(name = "content"))
    )
    Article article;


}
