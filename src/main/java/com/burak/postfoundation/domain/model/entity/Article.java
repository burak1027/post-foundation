package com.burak.postfoundation.domain.model.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Max;

@Embeddable
@Getter
@Setter
@ToString
public class Article {
    //long postId;
    //private String Title;
    @Max(10000)
    private String content;

}
