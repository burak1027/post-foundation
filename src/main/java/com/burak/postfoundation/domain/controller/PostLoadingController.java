package com.burak.postfoundation.domain.controller;

import com.burak.postfoundation.domain.model.dto.PostDto;
import com.burak.postfoundation.domain.service.PostLoadingService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/post")
@Api(value = "postLoading", produces = MediaType.APPLICATION_JSON_VALUE)

public class PostLoadingController {
    @Autowired
    PostLoadingService postLoadingService;

    @GetMapping(value = "/get-all-posts/{value}")
    public List<PostDto> getPosts(@Positive @PathVariable("value") int pageNum) {
        return postLoadingService.getPosts(pageNum);

    }

    @GetMapping(value = "/get-by-id/{value}")
    public PostDto getPostByPostId(@Positive @PathVariable("value") long id) {
        return postLoadingService.getPostByPostID(id);
    }

    @GetMapping(value = "/get-posts-by-user-id/{value}")
    public List<PostDto> getPostsOfAUser(@Positive @PathVariable("value") long id) {
        return postLoadingService.getPostsByUserId(id);
    }
    @GetMapping("get-posts-between-dates")
    public List<PostDto> getPostsBetweenDates(@Positive @RequestParam Date date, @RequestParam Date date2){
        return postLoadingService.getPostsBetweenDates(date,date2);
    }
    @GetMapping("/get-posts-by-category-id/{value}")
    public List<PostDto> getPostsByCategoryId(@Positive @PathVariable("value") long id){
        return postLoadingService.getPostsByCategory(id);
    }
    @GetMapping("/{value}")
    public PostDto getPostByPermalink(@NotNull @PathVariable("value") String permaLink){
        return postLoadingService.getByPermaLink(permaLink);
    }

}
