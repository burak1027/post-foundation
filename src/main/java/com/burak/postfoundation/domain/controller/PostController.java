package com.burak.postfoundation.domain.controller;

import com.burak.postfoundation.domain.model.dto.PostDto;
import com.burak.postfoundation.domain.service.PostService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/post")
@Api(value = "post", produces = MediaType.ALL_VALUE)
public class PostController {

    @Autowired
    PostService postService;

    @PostMapping()
    @ResponseBody
    public void createPost(@Valid @RequestBody PostDto postDto) {
        postService.createPost(postDto);
    }

    @PutMapping()
    @ResponseBody
    public void updatePost(@Valid @RequestBody PostDto postDto) {
        postService.updatePost(postDto);
    }

    @DeleteMapping(consumes = "application/json")
    @ResponseBody
    public void deletePost(@Valid @RequestBody PostDto postDto) {
        postService.deletePost(postDto);
    }

    @DeleteMapping(value = "/delete-by-id/{value}")
    @ResponseBody
    public void deletePostByPostId(@Positive @PathVariable("value") long id) {
        postService.deletePostByPostId(id);

    }

    @DeleteMapping(value = "/delete-by-user-id/{value}")
    @ResponseBody
    public void deletePostByUserId(@Positive @PathVariable("value") long id) {
        postService.deletePostByUserId(id);

    }

    @PutMapping(value = "/increment-viewed")
    @ResponseBody
    public void incrementViewed(PostDto postDto) {
        postService.incrementViewed(postDto);
    }

    @PutMapping(value = "/set-permission")
    @ResponseBody
    public void setPermisionView(PostDto postDto, @RequestParam boolean priv) {
        postService.setPrivate(postDto, priv);
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
