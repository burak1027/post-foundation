package com.burak.postfoundation.domain.service;

import com.burak.postfoundation.domain.model.dto.PostDto;
import com.burak.postfoundation.domain.model.entity.PostEntity;

public class PostMapper {
    public static PostDto entityToDto(PostEntity postEntity) {
        if (postEntity==null)
            return null;
        PostDto postDto = new PostDto();
        //{  postDto.setContent(postEntity.getContent());
        postDto.setDateCreated(postEntity.getDateCreated());
        postDto.setDeleted(postEntity.isDeleted());
        postDto.setId(postEntity.getId());
        postDto.setPrivate(postEntity.isPrivate());
        postDto.setLastUpdated(postEntity.getLastUpdated());
        postDto.setLikeCounter(postEntity.getLikeCounter());
        postDto.setTimesViewed(postEntity.getTimesViewed());
        postDto.setTitle(postEntity.getTitle());
        postDto.setPostCreatorId(postEntity.getPostCreatorId());
        postDto.setCategoryId(postEntity.getCategoryId());
        postDto.setArticle(postEntity.getArticle());
        postDto.setPermaLink(postEntity.getPermaLink());
        return postDto;
    }

    public static PostEntity dtoToEntity(PostDto postDto) {
        if (postDto==null)
            return null;
        PostEntity postEntity = new PostEntity();
        // postEntity.setContent(postDto.getContent());
        postEntity.setDateCreated(postDto.getDateCreated());
        postEntity.setDeleted(postDto.isDeleted());
        postEntity.setId(postDto.getId());
        postEntity.setPrivate(postDto.isPrivate());
        postEntity.setLastUpdated(postDto.getLastUpdated());
        postEntity.setLikeCounter(postDto.getLikeCounter());
        postEntity.setTimesViewed(postDto.getTimesViewed());
        postEntity.setTitle(postDto.getTitle());
        postEntity.setPostCreatorId(postDto.getPostCreatorId());
        postEntity.setCategoryId(postDto.getCategoryId());

        postEntity.setArticle(postDto.getArticle());

        postEntity.setPermaLink(postDto.getPermaLink());
        return postEntity;
    }
}
