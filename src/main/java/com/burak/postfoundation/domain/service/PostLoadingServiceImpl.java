package com.burak.postfoundation.domain.service;

//import com.burak.postfoundation.domain.VO.User;

import com.burak.postfoundation.domain.model.dto.PostDto;
import com.burak.postfoundation.domain.model.entity.PostEntity;
import com.burak.postfoundation.domain.repository.PostDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PostLoadingServiceImpl implements PostLoadingService {
    @Autowired
    PostDao postDao;

    @Override
    public List<PostDto> getPosts(int page) {
        List<PostDto> postList = new ArrayList<>();
        postDao.getAllPosts(page).forEach(postEntity -> postList.add(PostMapper.entityToDto(postEntity)));
        System.out.println(postList);
        return postList;
    }

    @Override
    @Transactional(readOnly = true)
    public PostDto getPostByPostID(long id) {
        //Atomic operations needed?
        PostDto postDto = PostMapper.entityToDto(postDao.getPostById(id));
        //postEntity.setTimesViewed(postEntity.getTimesViewed()+1);
        return postDto;
    }
//
//    @Override
//    public String creatorInfo(PostDto postDto) {
//      //  User user = null;
//        try(CloseableHttpClient httpClient = HttpClients.createDefault()){
//            ObjectMapper objectMapper = new ObjectMapper();
//            HttpGet request = new HttpGet("localhost:8080/user-foundation/user/"+postDto.getPostCreatorId());
//          //  user = httpClient.execute(request,classicHttpResponse ->
//           //         objectMapper.readValue(classicHttpResponse.getEntity().getContent(),User.class));
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
////        if(user==null){
////            //rollback
////        }
////        return user.toString();
//        return null;
//    }

//    @Override
//    public String creatorInfo(long postId) {
//        long id = postDao.getPostById(postId).getPostCreatorId();
//        User user = null;
//        try(CloseableHttpClient httpClient = HttpClients.createDefault()){
//            ObjectMapper objectMapper = new ObjectMapper();
//            HttpGet request = new HttpGet("http://localhost:8080/user-foundation/user/"+id);
//            user = httpClient.execute(request,classicHttpResponse ->
//                    objectMapper.readValue(classicHttpResponse.getEntity().getContent(),User.class));
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        if(user==null){
//            //rollback
//            return null;
//        }
//        return user.toString();
//    }
    @Transactional(readOnly = true)
    @Override
    public List<PostDto> getPostsBetweenDates(Date date1, Date date2) {
        List<PostDto> posts = new ArrayList<>();
        postDao.getPostsInATimePeriod(date1, date2).forEach(postEntity -> posts.add(PostMapper.entityToDto(postEntity)));
        return posts;
    }
    @Transactional(readOnly = true)
    @Override
    public List<PostDto> getPostsByUserId(long userId) {
        List<PostDto> list = new ArrayList<>();
        postDao.getPostsByUserId(userId).forEach(postEntity -> list.add(PostMapper.entityToDto(postEntity)));

        return list;
    }
    @Transactional(readOnly = true)
    @Override
    public List<PostDto> getPostsByCategory(Long id) {
        List<PostDto> postDtoList = new ArrayList<>();
        postDao.getPostsByCategoryId(id).forEach(postEntity -> postDtoList.add(PostMapper.entityToDto(postEntity)));
        return postDtoList;
    }
    @Transactional(readOnly = true)
    @Override
    public PostDto getByPermaLink(String name) {
        return PostMapper.entityToDto(postDao.getByPermaLink(name));
    }


}
