package com.burak.postfoundation.domain.service;

//import com.burak.postfoundation.domain.VO.User;

import com.burak.postfoundation.domain.model.dto.PostDto;
import com.burak.postfoundation.domain.model.entity.PostEntity;
import com.burak.postfoundation.domain.repository.PostDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    PostDao postDao;

    @Override
    @Transactional(propagation = Propagation.NESTED)
    public void createPost(PostDto postDto) {
        Date date = new Date();
        postDto.setDateCreated(date);
        postDto.setLastUpdated(date);
//        User user = null;
//        try(CloseableHttpClient httpClient = HttpClients.createDefault()){
//            ObjectMapper objectMapper = new ObjectMapper();
//            HttpGet request = new HttpGet("localhost:8080/user-foundation/user/"+postDto.getId());
//             user = httpClient.execute(request,classicHttpResponse ->
//                    objectMapper.readValue(classicHttpResponse.getEntity().getContent(),User.class));
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//       if(user==null){
//           //rollback
//       }
//        postDto.setAuthorName(user.getName()+" "+user.getSurname());
        PostEntity postEntity = PostMapper.dtoToEntity(postDto);
        postDao.createPost(postEntity);
    }

    @Override
    @Transactional
    public void updatePost(PostDto postDto) {
        Date date = new Date();
        postDto.setLastUpdated(date);
        postDao.updatePost(PostMapper.dtoToEntity(postDto));

    }

    @Override
    @Transactional
    public void deletePost(PostDto postDto) {
        postDao.deletePost(PostMapper.dtoToEntity(postDto));
    }

    @Override
    @Transactional
    public void deletePostByPostId(long id) {
        PostEntity postEntity = postDao.getPostById(id);
        postDao.deletePost(postEntity);
    }

    @Override
    @Transactional
    public void deletePostByUserId(long id) {
        List<PostEntity> postEntity = postDao.getPostsByUserId(id);
        postEntity.forEach(postEntity1 -> postDao.deletePost(postEntity1));
    }

    @Override
    @Transactional
    public void incrementViewed(PostDto postDto) {
        //Atomic operations needed?
        postDto.setTimesViewed(postDto.getTimesViewed() + 1);
        postDao.updatePost(PostMapper.dtoToEntity(postDto));
    }
    @Transactional
    @Override
    public void setPrivate(PostDto postDto, boolean value) {
        postDto.setPrivate(value);
        updatePost(postDto);
    }


}
