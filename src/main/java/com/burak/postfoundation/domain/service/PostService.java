package com.burak.postfoundation.domain.service;

import com.burak.postfoundation.domain.model.dto.PostDto;
import org.springframework.stereotype.Service;

@Service
public interface PostService {
    void createPost(PostDto postDto);

    void updatePost(PostDto postDto);

    void deletePost(PostDto postDto);

    void deletePostByPostId(long id);

    void deletePostByUserId(long id);

    void incrementViewed(PostDto postDto);

    void setPrivate(PostDto postDto, boolean value);


}
