package com.burak.postfoundation.domain.service;

import com.burak.postfoundation.domain.model.dto.PostDto;
import com.burak.postfoundation.domain.model.entity.PostEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

public interface PostLoadingService {
    List<PostDto> getPosts(int page);

    PostDto getPostByPostID(long id);

    //String creatorInfo(PostDto postDto);
    //String creatorInfo(long PostId);
    List<PostDto> getPostsBetweenDates(Date date1, Date date2);

    List<PostDto> getPostsByUserId(long userId);

    List<PostDto> getPostsByCategory(Long id);

    PostDto getByPermaLink(String  name);
    //List<PostEntity> getPostsByUserName(String name, String Surname);


}
