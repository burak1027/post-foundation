package com.burak.postfoundation.domain.repository;

import com.burak.postfoundation.domain.model.entity.PostEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Component
public class PostDaoImpl implements PostDao {

    @Autowired
    private EntityManager entityManager;


    @Override
    public List<PostEntity> getPostsByUserId(long id) {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("FROM PostEntity p WHERE p.postCreatorId = :id ");
        query.setParameter("id", id);
  //      query.executeUpdate();
        List<PostEntity> postList = query.list();
        return postList;
    }

    @Override
    public List<PostEntity> getPostsByCategory(String categoryName) {
        return null;
    }

    @Override
    public PostEntity getPostById(long id) {
        PostEntity postEntity = null;
        Session session = entityManager.unwrap(Session.class);
        postEntity = session.get(PostEntity.class, id);
        return postEntity;
    }

    @Override
    public List<PostEntity> getAllPosts(int pageNumber) {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("FROM PostEntity");
        int numberOfItemsInAPage = 10;
        query.setFirstResult((pageNumber - 1) * numberOfItemsInAPage);
        query.setMaxResults(numberOfItemsInAPage);
        List<PostEntity> postList = query.list();
        return postList;
    }



    @Override
    public List<PostEntity> getPostsInATimePeriod(Date date, Date date2) {
        System.out.println(date);
        System.out.println(date2);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String strDate = dateFormat.format(date);
        String strDate2 = dateFormat.format(date2);
        System.out.println(strDate2);
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("FROM PostEntity p WHERE p.dateCreated>= :date1 and p.dateCreated <= :date2");
        query.setParameter("date1", date, TemporalType.DATE);
        query.setParameter("date2", date2, TemporalType.DATE);


        //query.setParameter("date1", strDate);
        //query.setParameter("date2",strDate2);
        return query.list();
    }

    @Override
    public List<PostEntity> getPostsByCategoryId(Long id) {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("FROM PostEntity p WHERE p.categoryId = :id ");
        query.setParameter("id", id);
        List<PostEntity> postList = query.list();
        return postList;
    }

    @Override
    public PostEntity getByPermaLink(String permaLink) {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("FROM PostEntity p WHERE p.permaLink = :permaLink ");
        query.setParameter("permaLink", permaLink);
        List<PostEntity> postEntities =query.list();
        PostEntity postEntity = postEntities.size()==0?null:postEntities.get(0);
        return postEntity ;
    }

    @Override
    public void createPost(PostEntity postEntity) {
        Session session = entityManager.unwrap(Session.class);

        session.save(postEntity);

    }

    @Override
    public void updatePost(PostEntity postEntity) {
        Session session = entityManager.unwrap(Session.class);
        session.merge(postEntity);

    }

    @Override
    public void deletePost(PostEntity postEntity) {
        Session session = entityManager.unwrap(Session.class);
        session.remove(session.contains(postEntity) ? postEntity : session.merge(postEntity));

    }

}
