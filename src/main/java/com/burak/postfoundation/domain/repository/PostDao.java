package com.burak.postfoundation.domain.repository;

import com.burak.postfoundation.domain.model.entity.PostEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Component
public interface PostDao {
    List<PostEntity> getPostsByUserId(long id);

    List<PostEntity> getPostsByCategory(String categoryName);

    PostEntity getPostById(long id);

    List<PostEntity> getAllPosts(int pageNumber);

    List<PostEntity> getPostsInATimePeriod(Date date, Date date2);

    List<PostEntity> getPostsByCategoryId(Long id);

    PostEntity getByPermaLink(String permaLink);


    void createPost(PostEntity postEntity);

    void updatePost(PostEntity postEntity);

    void deletePost(PostEntity postEntity);


}
