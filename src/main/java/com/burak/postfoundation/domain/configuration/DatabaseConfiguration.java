//package com.burak.postfoundation.domain.configuration;
//
//import com.zaxxer.hikari.HikariConfig;
//import com.zaxxer.hikari.HikariDataSource;
//import org.hibernate.cfg.AvailableSettings;
//import org.hibernate.jpa.HibernatePersistenceProvider;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.core.env.Environment;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.spi.PersistenceProvider;
//import javax.sql.DataSource;
//import java.util.Properties;
//
//@Configuration
////@PropertySource("src/main/resources/application.yml")
//public class DatabaseConfiguration {
////    @Autowired
////    private Environment env;
//
//
//    @Bean
//    public static DataSource dataSource() {
//        HikariConfig config = new HikariConfig();
//        config.setJdbcUrl("jdbc:mysql://localhost:3306/UserDatabase");
//        config.setUsername("root");
//        config.setPassword("burak123");
//        config.addDataSourceProperty("cachePrepStmts", "true");
//        config.addDataSourceProperty("prepStmtCacheSize", "250");
//        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
//        HikariDataSource dataSource = new HikariDataSource(config);
//        return dataSource;
//    }
////    @Bean
////    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
////        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
////        em.setDataSource(dataSource());
////        em.setPackagesToScan("com.burak.postfoundation.domain.model");
////        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
////        //em.setJpaProperties(additionalProperties());
////        return em;
////    }
////
////    @Bean
////    JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
////        JpaTransactionManager transactionManager = new JpaTransactionManager();
////        transactionManager.setEntityManagerFactory(entityManagerFactory);
////        return transactionManager;
////    }
////
////    final Properties additionalProperties() {
////        final Properties hibernateProperties = new Properties();
////
//////        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
//////        hibernateProperties.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
//////        hibernateProperties.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
//////        hibernateProperties.setProperty("hibernate.cache.use_second_level_cache", env.getProperty("hibernate.cache.use_second_level_cache"));
//////        hibernateProperties.setProperty("hibernate.cache.use_query_cache", env.getProperty("hibernate.cache.use_query_cache"));
////
////        return hibernateProperties;
////    }
////private HibernateJpaVendorAdapter vendorAdaptor() {
////    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
////    vendorAdapter.setShowSql(true);
////    return vendorAdapter;
////}
//
////    @Bean
////    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(DataSource dataSource, PersistenceProvider persistenceProvider) {
////
////        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
////        entityManagerFactoryBean.setPackagesToScan("src/main");
////        entityManagerFactoryBean.setPersistenceUnitName("PostEntity");
////        entityManagerFactoryBean.setDataSource(dataSource);
////        entityManagerFactoryBean.setPersistenceProvider(persistenceProvider);
////        return entityManagerFactoryBean;
////    }
////@Bean(name="entityManagerFactory")
////public LocalSessionFactoryBean sessionFactory() {
////    LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
////    return sessionFactory;
////}
//
//
//}
