package com.burak.postfoundation;

import com.burak.postfoundation.domain.model.dto.PostDto;
import com.burak.postfoundation.domain.model.entity.Article;
import com.burak.postfoundation.domain.service.PostLoadingService;
import com.burak.postfoundation.domain.service.PostMapper;
import com.burak.postfoundation.domain.service.PostService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.util.Date;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.jupiter.api.Assertions.*;



@SpringBootTest
//@TestPropertySource("classpath:application-test.properties")
@TestPropertySource(locations= "classpath:application-test.yml")
@ActiveProfiles("test")
@AutoConfigureMockMvc
@WebAppConfiguration
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class IntegrationTest {
    MockMvc mockMvc;
    PostService postService;
    PostLoadingService postLoadingService;

    @Autowired
    public IntegrationTest(MockMvc mockMvc, PostService postService, PostLoadingService postLoadingService) {
        this.mockMvc = mockMvc;
        this.postService = postService;
        this.postLoadingService = postLoadingService;
    }

    public  String asJsonString(PostDto postDto) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(postDto);
        return requestJson;

    }

    @Test
    @Order(1)
    void createPost() {
        for (int i = 1; i < 33; i++) {
            //postDao = new PostDaoImpl();
            Date date = new Date();
            Article article = new Article();
            article.setContent("some random text");
            PostDto postDto = PostDto.builder().dateCreated(date).lastUpdated(date).id((long) i)
                    .isDeleted(false).isPrivate(false).article(article).title("Randomized").likeCounter(0)
                    .postCreatorId((long) i / 10).categoryId(i%5).permaLink("post-no-"+(i)).build();
            postService.createPost(postDto);
            System.out.println(postDto.getPermaLink()+"     "+postDto.getId());

        }

    }

    @Order(2)
    @Test
    public void test() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/post/get-by-id/6"))
                .andDo(print()).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("6"));
    }
    @Order(3)
    @Test
    public void getByPostIdMockMvcTest() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/post/get-posts-by-user-id/2"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
    @Order(4)
    @Test
    public void getByCategoryIdMockMvcTest() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/post/get-posts-by-category-id/2")).andDo(print()).andExpect(MockMvcResultMatchers.status().isOk());
    }
    @Order(5)
    @Test
    public void createPostMockMvcTest() throws Exception{
        Date date = new Date();
        int i = 80;
        Article article = new Article();
        article.setContent("some random text");
        PostDto postDto = PostDto.builder().dateCreated(date).lastUpdated(date).id((long) i)
                .isDeleted(false).isPrivate(false).article(article).title("Randomized").likeCounter(0)
                .postCreatorId((long) i/10 + 1).categoryId(i%5+1).permaLink("link-to-post").build();

        mockMvc.perform(MockMvcRequestBuilders.post("/post").contentType(APPLICATION_JSON).content(asJsonString(postDto))).andDo(print()).andExpect(MockMvcResultMatchers.status().isOk());
        assertNotEquals(postLoadingService.getPostByPostID(80L),null);
    }
    @Order(6)
    @Test
    public void deletePostMockMvcTest() throws Exception {
        Date date = new Date();
        int i = 80;
        Article article = new Article();
        article.setContent("some random text");
        PostDto postDto = PostDto.builder().dateCreated(date).lastUpdated(date).id((long) i)
                .isDeleted(false).isPrivate(false).article(article).title("Randomized").likeCounter(0)
                .postCreatorId((long) i/10 + 1).categoryId(i%5+1).permaLink("link-to-post").build();

        mockMvc.perform(MockMvcRequestBuilders.delete("/post").contentType(APPLICATION_JSON).content(asJsonString(postDto)))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        System.out.println(postLoadingService.getPostByPostID(80L));
        //assertEquals(postLoadingService.getPostByPostID(80L),null);
    }
    @Order(7)
    @Test
    public void updatePostMockMvcTest() throws Exception{
        Date date = new Date();
        int i = 22;
        Article article = new Article();
        article.setContent("some random text");
        PostDto postDto = PostDto.builder().dateCreated(date).lastUpdated(date).id((long) i)
                .isDeleted(false).isPrivate(false).article(article).title("New title here").likeCounter(0)
                .postCreatorId((long) i / 10).categoryId(i%5).permaLink("post-no-"+(i)).build();

        mockMvc.perform(MockMvcRequestBuilders.put("/post").contentType(APPLICATION_JSON).content(asJsonString(postDto))).andDo(print()).andExpect(MockMvcResultMatchers.status().isOk());
        assertEquals(postLoadingService.getPostByPostID(22L).getTitle(),"New title here");
    }
    @Order(8)
    @Test
    public void deleteById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/post/delete-by-id/10").contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        assertNull(postLoadingService.getPostByPostID(10));
    }
    @Order(9)
    @Test
    public void getPostByPermalink() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/post/post-no-27"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("27"));
   }

}
