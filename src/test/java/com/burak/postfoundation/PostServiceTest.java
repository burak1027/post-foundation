package com.burak.postfoundation;

import com.burak.postfoundation.domain.model.dto.PostDto;
import com.burak.postfoundation.domain.model.entity.Article;
import com.burak.postfoundation.domain.service.PostLoadingService;
import com.burak.postfoundation.domain.service.PostMapper;
import com.burak.postfoundation.domain.service.PostService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import java.util.Date;
import java.util.List;
//
@TestPropertySource(locations= "classpath:application-test.yml")
@ActiveProfiles("test")
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class PostServiceTest {

    PostService postService;
    PostLoadingService postLoadingService;
    @Autowired
    public PostServiceTest(PostService postService, PostLoadingService postLoadingService) {
        this.postService = postService;
        this.postLoadingService = postLoadingService;
    }
    @Order(1)
    @Test
    void createPost() {
        System.out.println(postLoadingService.getPostByPostID(0));
        for (int i = 0; i < 33; i++) {
            System.out.println(i);
            //postDao = new PostDaoImpl();
            Date date = new Date();
            Article article = new Article();
            article.setContent("some random text");
            PostDto postDto = PostDto.builder().dateCreated(date).lastUpdated(date).id(i)
                    .isDeleted(false).isPrivate(false).article(article).title("Randomized").likeCounter(0)
                    .postCreatorId((long) i / 10).categoryId(i%5).permaLink("permalink-"+i).build();
            //System.out.println(postDto.toString());
            //postService.createPost(postDto);
            postService.createPost(postDto);

        }
        //System.out.println(postDao.getPostById(4L));

    }

    @Order(2)
    @Test
    void  getByPostIdAndUpdateByService(){
        //createPost();
        PostDto postDto = postLoadingService.getPostByPostID(1L);
        System.out.println(postDto.getTitle());
        postDto.setTitle("new Title");
        System.out.println(postLoadingService.getPostByPostID(1L).getTitle());
        postService.updatePost(postDto);
        System.out.println(postDto.getTitle());
    }
    @Order(3)
    @Test
    void deletePostByUserId(){
        //createPost();
        List<PostDto> postDtoList = postLoadingService.getPostsByUserId(1L);
        postDtoList.forEach(postDto -> System.out.println(postDto.getId()));
        postService.deletePostByUserId(1L);
        postDtoList = postLoadingService.getPostsByUserId(1L);
        System.out.println(postDtoList.size());



    }
    @Order(4)
    @Test
    void getPostAndIncrementViewed(){
        //createPost();
        PostDto postDto = postLoadingService.getPostByPostID(1L);
        System.out.println(postDto.getTimesViewed());
        postService.incrementViewed(postDto);
        postDto = postLoadingService.getPostByPostID(1L);
        System.out.println(postDto.getId());
        System.out.println(postDto.getTimesViewed());
    }
    @Order(5)
    @Test
    void setPrivateTest(){
        //createPost();
        PostDto postDto = postLoadingService.getPostByPostID(3L);
        System.out.println(postDto.isPrivate());
        postService.setPrivate(postDto,true);
        System.out.println(postLoadingService.getPostByPostID(3L).isPrivate());

    }
}
