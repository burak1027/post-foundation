//package com.burak.postfoundation;
//
//import com.burak.postfoundation.domain.model.dto.PostDto;
//import com.burak.postfoundation.domain.model.entity.Article;
//import com.burak.postfoundation.domain.model.entity.PostEntity;
//import com.burak.postfoundation.domain.repository.PostDao;
//import com.burak.postfoundation.domain.service.PostLoadingService;
//import com.burak.postfoundation.domain.service.PostMapper;
//import com.burak.postfoundation.domain.service.PostService;
//import org.junit.jupiter.api.MethodOrderer;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestMethodOrder;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.support.AnnotationConfigContextLoader;
//import org.springframework.transaction.annotation.Transactional;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//import java.util.Date;
//
////@DataJpaTest
////@ActiveProfiles("test")
////@DataJpaTest
////@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
////@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
////@RunWith(SpringJUnit4ClassRunner.class)
////@RunWith(SpringRunner.class)
////@ContextConfiguration(
////        classes = { DatabaseConfiguration.class },  loader = AnnotationConfigContextLoader.class)
//
//@Transactional
////@ContextConfiguration(
////        classes = { DatabaseTestConfig.class },
////        loader = AnnotationConfigContextLoader.class)
//@TestPropertySource(locations= "classpath:application-test.yml")
//@ActiveProfiles("test")
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
//
//
//@SpringBootTest
//public class PostTest {
//
//    PostDao postDao;
//    //@AfterEach
//    PostService postService;
//    PostLoadingService postLoadingService;
//
//    @Autowired
//    public PostTest(PostDao postDao, PostService postService, PostLoadingService postLoadingService) {
//        this.postDao = postDao;
//        this.postService = postService;
//        this.postLoadingService = postLoadingService;
//    }
//    @Order(1)
//
//    void createPost() {
//        for (int i = 0; i < 33; i++) {
//            //postDao = new PostDaoImpl();
//            Date date = new Date();
//            Article article = new Article();
//            article.setContent("some random text");
//            PostDto postDto = PostDto.builder().dateCreated(date).lastUpdated(date).id((long) i)
//                    .isDeleted(false).isPrivate(false).article(article).title("Randomized").likeCounter(0)
//                    .postCreatorId((long) i / 10).categoryId(i%5).build();
//            //System.out.println(postDto.toString());
//            //postService.createPost(postDto);
//            System.out.println(postDto.getPostCreatorId());
//            postDao.createPost(PostMapper.dtoToEntity(postDto));
//
//        }
//        //System.out.println(postDao.getPostById(4L));
//
//    }
//    @Order(2)
//    @Test
//    void updatePost() {
//        PostEntity postEntity = postDao.getPostById(4L);
//        postEntity.setPostCreatorId(7L);
//        postDao.updatePost(postEntity);
//    }
//    @Order(3)
//    @Test
//    void deletePost() {
//        PostEntity postEntity = postDao.getPostById(4L);
//        postDao.deletePost(postEntity);
//
//    }
//    @Order(4)
//    @Test
//    void postCreateUpdateAndDelete() {
//        createPost();
//        updatePost();
//        System.out.println(postDao.getPostById(4L));
//        deletePost();
//        System.out.println(postDao.getPostById(4L));
//
//
//    }
//    @Order(5)
//    @Test
//    void getAll() {
//        createPost();
//        postDao.getAllPosts(1).forEach(postEntity -> System.out.println(postEntity));
//    }
//    @Order(6)
//    @Test
//    void getByUserId() {
//        createPost();
//        postDao.getPostsByUserId(1).forEach(postEntity ->
//                //assertEquals(postEntity.getPostCreatorId(), 1L)
//                System.out.println(postEntity.getPostCreatorId())
//        );
//
//
//    }
//    @Order(7)
//    @Test
//    void getByCategoryId(){
//        createPost();
//        postDao.getPostsByCategoryId(3L).forEach(postEntity ->
//                System.out.println(postEntity.getId()));
//    }
//    @Order(8)
//    @Test
//    void getByPostId(){
//        createPost();
//        System.out.println(postDao.getPostById(5).toString());
//    }
//    @Order(9)
//    @Test
//    void getByPostIdAndUpdateForDAO(){
//        createPost();
//        PostEntity postEntity = postDao.getPostById(1L);
//        System.out.println(postEntity.getTitle());
//        postEntity.setTitle("new title");
//        System.out.println(postDao.getPostById(1L).getTitle());
//        postDao.updatePost(postEntity);
//        System.out.println(postDao.getPostById(1L).getTitle());
//
//
//    }
//
//
//}
