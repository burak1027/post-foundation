//package com.burak.postfoundation;
//
//import com.zaxxer.hikari.HikariConfig;
//import com.zaxxer.hikari.HikariDataSource;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.JpaVendorAdapter;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.Persistence;
//import javax.sql.DataSource;
//
//@Configuration
//@EnableTransactionManagement
//
//public class DatabaseConfiguration {
//    @Bean
//    public static DataSource dataSource() {
//        HikariConfig config = new HikariConfig();
//        config.setDataSourceClassName("org.h2.jdbcx.JdbcDataSource");
//        config.setConnectionTestQuery("VALUES 1");
//        config.addDataSourceProperty("URL", "jdbc:h2:~/test");
//         HikariDataSource dataSource = new HikariDataSource(config);
//        return dataSource;
//    }
//    @Bean
//    public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
//        JpaTransactionManager transactionManager = new JpaTransactionManager();
//        transactionManager.setEntityManagerFactory(emf);
//
//        return transactionManager;
//    }
//    @Bean
//    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
//        em.setDataSource(dataSource());
//        em.setPackagesToScan(new String[] { "\\your package here" });
//
//        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//        em.setJpaVendorAdapter(vendorAdapter);
//        //em.setJpaProperties(additionalProperties());
//
//        return em;
//    }
//    public static EntityManager getEntityManager() {
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.burak.post");
//
//        return emf.createEntityManager();
//    }
//
//}
